import { Knex } from 'knex';
export class ServiceModels {
  /**
   * บันทึกข้อมูล Admit
   * @param db 
   * @param data 
   * @returns 
   */

/**
 * ข้อมูลทั่วไปผู้รับบริการ HIS 
 * @param db 
 * @param an 
 * @param limit 
 * @param offset 
 * @returns 
 */
  patient(db: Knex,an: string, limit: number, offset: number) {
    if(an){
      let sql:any = db.raw(`SELECT i.an , i.hn ,p.cid,p.pname AS title , p.fname,p.lname,s.name AS gender,p.birthday AS dob 
      ,CONCAT(a.age_y,"-",a.age_m,"-",a.age_d) AS age
      , n.name AS nationality,ci.emp_citizenship_name AS citizenship 
      ,r.name AS religion , m.name AS married_status,oc.name AS occupation
      ,p.bloodgrp AS blood_group , p.pttype AS inscl , pt.name AS inscl_name
      ,CONCAT(p.addrpart,(" หมู่ "),p.moopart,(" ต. "),t1.name ,(" อ. "),t2.name ,(" จ. "),t3.name) AS address ,p.hometel AS phone
      ,if(op.pregnancy = "Y","TRUE","FALSE") as is_pregnant
      , p.informname AS reference_preson, informtel AS reference_phone , informaddr  AS reference_address,p.informrelation AS reference_relationship
      , i.regdate as admit_date,i.regtime as admit_time,d.name as admit_by
      ,GROUP_CONCAT(DISTINCT(odd.diag_text)) as pre_diag
      FROM ipt i
      LEFT OUTER JOIN patient p ON p.hn = i.hn
      LEFT OUTER JOIN sex s ON s.code = p.sex
      LEFT OUTER JOIN occupation oc ON oc.occupation = p.occupation
      LEFT OUTER JOIN nationality n ON n.nationality = p.nationality
      LEFT OUTER JOIN emp_citizenship ci ON ci.emp_citizenship_id = p.citizenship
      LEFT OUTER JOIN religion r ON r.religion = p.religion
      LEFT OUTER JOIN marrystatus m ON m.code = p.marrystatus
      LEFT OUTER JOIN pttype pt ON pt.pttype = p.pttype
      LEFT OUTER JOIN opdscreen op ON op.vn = i.vn 
      LEFT OUTER JOIN an_stat a ON a.an = i.an
      LEFT OUTER JOIN doctor d on d.code = i.admdoctor
      LEFT OUTER JOIN ovst_doctor_diag odd on odd.vn = i.vn
      LEFT OUTER JOIN thaiaddress t1 on (t1.tmbpart = p.tmbpart AND t1.amppart = p.amppart AND t1.chwpart = p.chwpart)
      LEFT OUTER JOIN thaiaddress t2 on (t2.tmbpart = "00" AND t2.amppart = p.amppart AND t2.chwpart = p.chwpart)
      LEFT OUTER JOIN thaiaddress t3 on (t3.tmbpart = "00" AND t3.amppart = "00" AND t3.chwpart = p.chwpart)
      WHERE i.dchdate IS NULL AND i.an = ?
      GROUP BY i.an`,[an]);
      return sql;
    }else{
      let sql:any = db.raw(`SELECT i.an , i.hn ,p.cid,p.pname AS title , p.fname,p.lname,s.name AS gender,p.birthday AS dob 
      ,CONCAT(a.age_y,"-",a.age_m,"-",a.age_d) AS age
      , n.name AS nationality,ci.emp_citizenship_name AS citizenship 
      ,r.name AS religion , m.name AS married_status,oc.name AS occupation
      ,p.bloodgrp AS blood_group , p.pttype AS inscl , pt.name AS inscl_name
      ,CONCAT(p.addrpart,(" หมู่ "),p.moopart,(" ต. "),t1.name ,(" อ. "),t2.name ,(" จ. "),t3.name) AS address ,p.hometel AS phone
      ,if(op.pregnancy = "Y","TRUE","FALSE") as is_pregnant
      , p.informname AS reference_preson, informtel AS reference_phone , informaddr  AS reference_address,p.informrelation AS reference_relationship
      , i.regdate as admit_date,i.regtime as admit_time,d.name as admit_by
      ,GROUP_CONCAT(DISTINCT(odd.diag_text)) as pre_diag
      FROM ipt i
      LEFT OUTER JOIN patient p ON p.hn = i.hn
      LEFT OUTER JOIN sex s ON s.code = p.sex
      LEFT OUTER JOIN occupation oc ON oc.occupation = p.occupation
      LEFT OUTER JOIN nationality n ON n.nationality = p.nationality
      LEFT OUTER JOIN emp_citizenship ci ON ci.emp_citizenship_id = p.citizenship
      LEFT OUTER JOIN religion r ON r.religion = p.religion
      LEFT OUTER JOIN marrystatus m ON m.code = p.marrystatus
      LEFT OUTER JOIN pttype pt ON pt.pttype = p.pttype
      LEFT OUTER JOIN opdscreen op ON op.vn = i.vn 
      LEFT OUTER JOIN an_stat a ON a.an = i.an
      LEFT OUTER JOIN doctor d on d.code = i.admdoctor
      LEFT OUTER JOIN ovst_doctor_diag odd on odd.vn = i.vn
      LEFT OUTER JOIN thaiaddress t1 on (t1.tmbpart = p.tmbpart AND t1.amppart = p.amppart AND t1.chwpart = p.chwpart)
      LEFT OUTER JOIN thaiaddress t2 on (t2.tmbpart = "00" AND t2.amppart = p.amppart AND t2.chwpart = p.chwpart)
      LEFT OUTER JOIN thaiaddress t3 on (t3.tmbpart = "00" AND t3.amppart = "00" AND t3.chwpart = p.chwpart)
      WHERE i.dchdate IS NULL
      GROUP BY i.an`);
      return sql;
    }
  }

  review(db: Knex,an: string) {
    let sql:any = db.raw(`SELECT i.an , i.vn , op.cc AS chieft_complaint , op.symptom AS present_illness 
    ,CONCAT(IFNULL(op.pmh,SPACE(1)),IFNULL(op.fh,SPACE(1)),IFNULL(op.sh,SPACE(1))) AS past_history, op.pe AS physical_exam
    ,op.bw AS body_weight ,op.height AS body_height , op.pulse AS pulse_rate , op.rr AS respiratory_rate
    ,op.temperature AS body_temperature, op.bps AS systolic_blood_pressure , op.bpd AS diastolic_blood_pressure , " " AS oxygen_sat
    ," " AS eye_score , " " AS movement_score , " " AS verbal_score,op.vstdate AS visit_date, op.vsttime AS visit_time
    FROM ipt i
    LEFT OUTER JOIN opdscreen op ON op.vn = i.vn
    WHERE i.an = ?
    group by i.an`,[an]);
    return sql;
  }

  treatement(db: Knex,an: string) {
    let sql:any = db.raw(`SELECT i.an , op.income AS item_type_id , op.icode AS item_code , s.name AS item_name, dg.shortlist AS item_describe
    ,op.sum_price AS price , op.qty AS quantity , op.rxdate AS treatment_date , op.rxtime AS treatment_time,IFNULL(ou.name,d.name) AS treatment_by		
    FROM ipt i
    LEFT OUTER JOIN opitemrece op ON op.an = i.an
    LEFT OUTER JOIN s_drugitems s ON s.icode = op.icode
    LEFT OUTER JOIN drugusage dg ON dg.drugusage = op.drugusage
    LEFT OUTER JOIN opduser ou ON ou.loginname = op.staff
    LEFT OUTER JOIN doctor d on d.code = i.admdoctor
    WHERE i.an = ?`,[an]);
    return sql;
  }

  admit(db: Knex,an: string) {
    let sql:any = db.raw(`SELECT i.an,i.hn,i.vn
    ,i.regdate as admit_date,i.regtime as admit_time
    ,d.name as admit_by
    ,i.spclty as department_code
    ,i.ward as ward_code
    ,ia.bedtype as bed_code,ia.bedno as bed_number
    ,i.pttype as inscl_code,pt.name as inscl_name
    ,o.diag_text as pre_diag
    FROM ipt i
    LEFT OUTER JOIN doctor d on d.code = i.admdoctor
    LEFT OUTER JOIN ipt_spclty ip on ip.ipt_spclty = i.spclty
    LEFT OUTER JOIN ward w on w.ward = i.ward 
    LEFT OUTER JOIN ovst o on o.vn = i.vn
    LEFT OUTER JOIN pttype pt on pt.pttype = i.pttype
    LEFT OUTER JOIN iptadm ia on ia.an = i.an
    WHERE i.an = ?
    GROUP BY i.an`,[an]);
    return sql;
  }  

  lab_result(db: Knex,an: string,items_code:string) {
    let sql:any = db.raw(`
    SELECT  if(lisg.lab_items_sub_group_code is null,(CONCAT("S",lo.lab_items_code )),CONCAT("G",lisg.lab_items_sub_group_code)) as code
			 ,ifnull(lisg.lab_items_sub_group_name,lo.lab_items_name_ref) as name
			 ,lo.lab_items_name_ref as lab_test_name
			 ,lo.lab_order_result as lab_test_result
			 ,li.lab_items_unit as lab_test_unit
			 ,lo.lab_items_normal_value_ref as lab_test_ref_range
			 ,lh.report_date as lab_test_result_date
			 ,lh.report_time as lab_test_result_time
			 ,ou.name as _lab_test_result_by
			 ,lh.vn as an
    FROM lab_head lh 
    INNER JOIN lab_order lo on lo.lab_order_number = lh.lab_order_number and lo.confirm = 'Y'
    LEFT OUTER JOIN lab_items li on li.lab_items_code = lo.lab_items_code
    LEFT OUTER JOIN opduser ou on ou.loginname = lh.reporter_name
    LEFT OUTER JOIN lab_items_sub_group lisg on lisg.lab_items_sub_group_code = lo.lab_items_sub_group_code
    WHERE lh.department = "IPD"
		and lh.vn = ?
		and if(lisg.lab_items_sub_group_code is null,(CONCAT("S",lo.lab_items_code )),CONCAT("G",lisg.lab_items_sub_group_code)) = ?
    ORDER BY lh.report_date`,[an,items_code]);    
    return sql;
  }
  
  ekg_file(db: Knex,an: string) {
    let sql:any = db.raw(`SELECT n.icode as code ,n.name,"" as image_url
    FROM nondrugitems n
    INNER JOIN opitemrece op on op.icode = n.icode
    WHERE n.name LIKE "%EKG%"
    AND op.an = ?`,[an]);    
    return sql;
  }  

  patient_allergy(db: Knex,an: string) {
    let sql:any = db.raw(`SELECT oa.hn,oa.agent as drug_name
    ,ar.relation_name as allergy_level
    ,oa.symptom,oa.report_date as allergy_date
    FROM opd_allergy oa
    INNER JOIN ipt i on i.hn = oa.hn
    LEFT OUTER JOIN allergy_relation ar on ar.allergy_relation_id = oa.allergy_relation_id
    WHERE i.an = ?
    GROUP BY oa.agent`,[an]);    
    return sql;
  } 
}