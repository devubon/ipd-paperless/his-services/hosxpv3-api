import { Knex } from 'knex';
export class ServiceModels {
  items(db: Knex) {
    let sql:any = db.raw(`(SELECT d.icode as code,REPLACE(REPLACE(REPLACE(CONCAT(d.name,SPACE(1),d.strength),"'",''),'"',''),"-",'') as name ,d.income as item_type_id
    ,d.units as unit
    ,concat('[{"code":"TMLT","value":"',"",'"},{"code":"TMT","value":"',IFNULL(d.tpu_code_list,SPACE(1)),'"},{"code":"STD24","value":"',IFNULL(d.did,SPACE(1)) ,'"}]')as reference_code
    ,concat('[{"code":"price1","value":"',IFNULL(d.ipd_price,0),'"},{"code":"price2","value":"',IFNULL(d.ipd_price2,0),'"},{"code":"price3","value":"',IFNULL(d.ipd_price3,0),'"}]')as price
FROM drugitems d
)
UNION
(SELECT n.icode as code,REPLACE(REPLACE(REPLACE(n.name,"'",''),'"',''),"-",'') as name ,n.income as item_type_id
    ,n.unit as unit
    ,concat('[{"code":"CGD","value":"',IFNULL(n.nhso_adp_code,SPACE(1)),'"},{"code":"TMLT","value":"',IFNULL(n.tmlt_code,SPACE(1)),'"},{"code":"SIMB","value":"',IFNULL(n.simb_2005,SPACE(1)),'"}]')as reference_code
    ,concat('[{"code":"price1","value":"',IFNULL(n.ipd_price,0),'"},{"code":"price2","value":"',IFNULL(n.ipd_price2,0),'"},{"code":"price3","value":"',IFNULL(n.ipd_price3,0),'"}]')as price
FROM nondrugitems n
)
UNION
(SELECT IFNULL(CONCAT("G",lis.lab_items_sub_group_code),CONCAT("S",li.lab_items_code)) as code
    ,IFNULL(lisg.lab_items_sub_group_name,li.lab_items_name) as name,"07" as item_type_id,li.lab_items_unit as unit
    ,concat('[{"code":"ICODE","value":"',IFNULL((IFNULL(lisg.group_icode,li.icode)),SPACE(1)),'"},{"code":"TMLT","value":"',IFNULL(li.tmlt_code,SPACE(1)),'"},{"code":"LOINC_CODE","value":"',IFNULL(li.loinc_code,SPACE(1)),'"},{"code":"SIMB","value":"',"",'"}]')as reference_code
    ,concat('[{"code":"price1","value":"',IFNULL((IFNULL(lisg.group_price_ipd,li.service_price_ipd)),0),'"},{"code":"price2","value":"',IFNULL((IFNULL(lisg.group_price_ipd2,li.service_price_ipd2)),0),'"},{"code":"price3","value":"',IFNULL((IFNULL(lisg.group_price_ipd3,li.service_price_ipd3)),0),'"}]')as price
from  lab_items  li
LEFT OUTER JOIN lab_items_sub_group_list lis on lis.lab_items_code = li.lab_items_code
LEFT OUTER JOIN lab_items_sub_group lisg on lisg.lab_items_sub_group_code = lis.lab_items_sub_group_code
GROUP BY IFNULL(CONCAT("G",lis.lab_items_sub_group_code),CONCAT("S",li.lab_items_code))
)
UNION
(SELECT CONCAT("X",xi.xray_items_code) as code ,xi.xray_items_name as name 
    ,"08" as item_type_id,"ครั้ง" as unit
    ,concat('[{"code":"ICODE","value":"',IFNULL(xi.icode,SPACE(1)),'"},{"code":"TMLT","value":"'," ",'"},{"code":"LOINC_CODE","value":"',IFNULL(xi.loinc_code,SPACE(1)),'"},{"code":"SIMB","value":"',"",'"}]')as reference_code
    ,concat('[{"code":"price1","value":"',IFNULL(xi.service_price_ipd,0),'"},{"code":"price2","value":"',IFNULL(xi.service_price_ipd2,0),'"},{"code":"price3","value":"',IFNULL(xi.service_price_ipd3,0),'"}]')as price
FROM xray_items xi
)
`);    
return sql;
}

  insurance(db: Knex) {
    let sql:any = db.raw(`SELECT pt.pttype as code,pt.name
    ,concat('[{"code":"PCODE","name":"',IFNULL(pt.pcode,SPACE(1)),'"},{"code":"ECLAIM","name":"',IFNULL(pt.pttype_eclaim_id,SPACE(1)),'"},{"code":"NHSO","name":"',IFNULL(pt.nhso_code,SPACE(1)),'"},{"code":"STDCODE","name":"',IFNULL(pt.pttype_std_code,SPACE(1)),'"},{"code":"HIPDATA","name":"',IFNULL(pt.hipdata_code,SPACE(1)),'"},{"code":"AIPN","name":"',IFNULL(pt.pttype_eclaim_id,SPACE(1)),'"},{"code":"INSCL","name":"',IFNULL(if(pt.price_type = "0" ,"price1",if(pt.price_type = "1","price2","price3")),SPACE(1)),'"}]') as reference_code
from pttype pt`);    
return sql;
}

  ward(db: Knex) {
    let sql:any = db.raw(`SELECT w.ward as code,w.name ,'' as department_id,'' as bed_amount FROM ward w`);    
    return sql;
  }

  department(db: Knex) {
    let sql:any = db.raw(`SELECT i.ipt_spclty as code ,i.name FROM ipt_spclty i`);    
    return sql;
  }

  bed(db: Knex) {
    let sql:any = db.raw(`SELECT bt.name,IFNULL(b.room_charge_price,0) as standard_price,0 as extra_paid
    FROM bedno b
    INNER JOIN bedtype bt on bt.bedtype = b.bedtype
    GROUP BY bt.bedtype,b.room_charge_price`);    
    return sql;
  }

  food(db: Knex) {
    let sql:any = db.raw(`select f.food_menu_head_id as code,f.menu_name as name
    from food_menu_head f
    order by f.menu_name`);    
    return sql;
  }

  medicine(db: Knex) {
    let sql:any = db.raw(`SELECT d.icode as code,d.name,d.strength,d.units as unit
    ,d.sks_product_category_id as product_cat,d.drugusage as default_usage
    ,'' as default_route,if(d.drugaccount is null,"TRUE","FALSE") as is_ed
    ,CONCAT(d.name,SPACE(1),d.strength) as tallman, 1 as medicine_type_id
    ,if(d.pregnancy is NULL,"FALSE","TRUE") as is_prenant_allow
    from drugitems d`);    
    return sql;
  }

  medicine_usage(db: Knex) {
    let sql:any = db.raw(`SELECT du.drugusage as code,du.code as name
    ,du.name1  as description,'' as time_per_day
    FROM drugusage du`);    
    return sql;
  }

  refer_cause(db: Knex) {
    let sql:any = db.raw(`SELECT rc.id as code,rc.name from refer_cause rc`);    
    return sql;
  }

  refer_by(db: Knex) {
    let sql:any = db.raw(`
    SELECT r.code,r.name
    FROM(
    SELECT if(CONCAT(IFNULL(r.with_nurse,''),IFNULL(r.with_ambulance,'')) = "NN",0,
              if(CONCAT(IFNULL(r.with_nurse,''),IFNULL(r.with_ambulance,'')) = "NY",1,
                  if(CONCAT(IFNULL(r.with_nurse,''),IFNULL(r.with_ambulance,'')) = "YY",2,0))) as code
    
           ,CONVERT(if(CONCAT(IFNULL(r.with_nurse,''),IFNULL(r.with_ambulance,'')) = "NN","ไปเอง",
             if(CONCAT(IFNULL(r.with_nurse,''),IFNULL(r.with_ambulance,'')) = "NY","ส่งต่อด้วยรถ Ambulance",
                if(CONCAT(IFNULL(r.with_nurse,''),IFNULL(r.with_ambulance,'')) = "YY","ส่งต่อด้วยรถ Ambulance พร้อม พยาบาล","ไปเอง"))) using 'utf8') as name
    FROM referout r
    GROUP BY r.vn
    ) r
    GROUP BY r.name
    `);    
    return sql;
  }

  specialist(db: Knex) {
    let sql:any = db.raw(`SELECT s.spclty as code ,s.name FROM spclty s ORDER BY s.name`);    
    return sql;
  }
 
}